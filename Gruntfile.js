module.exports = function(grunt) {
 
  grunt.registerTask('watch', [ 'watch' ]);
 
  grunt.initConfig({

  	tishadow: {
  		
	    run: {
			command: 'run',
      options: {
        platform: 'ios'
        },
			
    	},
    	update: {
			command: 'run',
			options: {
				update: true,
				platform: 'ios'
	  		},
    	}
  	},
    watch: {
      js: {
        files: ['app/**/*.js', 'app/**/*.xml'],
        tasks: ['tishadow:update'],
        options: {
        }
      },
      tss: {
        files: ['app/**/*.tss'],
        tasks: ['tishadow:run'],
        options: {
        }
      }
    }

  });

  grunt.loadNpmTasks('grunt-tishadow');
  grunt.loadNpmTasks('grunt-contrib-watch');
 
};
