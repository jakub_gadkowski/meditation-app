exports.helpScreens = {
	'ios': {
		'iphone' : {
			'imagePath' : '/helpScreens/ios/iphone/',
			'items' : [
				{
					'screenName' : 'Home Screen',
					'images' : [
						'helpScreens-iphone1.jpg',
						'helpScreens-iphone2.jpg',
						'helpScreens-iphone3.jpg'
					]
				},
				{
					'screenName' : 'Meditation List',
					'images' : [
						'helpScreens-iphone4.jpg',
						'helpScreens-iphone5.jpg',
						'helpScreens-iphone6.jpg',
						'helpScreens-iphone7.jpg',
						'helpScreens-iphone8.jpg',
						'helpScreens-iphone9.jpg'
					]
				},
				{
					'screenName' : 'Meditation Settings',
					'images' : [
						'helpScreens-iphone10.jpg',
						'helpScreens-iphone11.jpg'
					]
				},
				{
					'screenName' : 'Interval Settings',
					'images' : [
						'helpScreens-iphone12.jpg',
						'helpScreens-iphone13.jpg',
						'helpScreens-iphone14.jpg'
					]
				},
				{
					'screenName' : 'Sounds',
					'images' : [
						'helpScreens-iphone15.jpg',
						'helpScreens-iphone16.jpg'
					]
				},
				{
					'screenName' : 'Animations',
					'images' : [
						'helpScreens-iphone17.jpg'
					]
				},
				{
					'screenName' : 'Meditation Length/Start Delay',
					'images' : [
						'helpScreens-iphone18.jpg',
						'helpScreens-iphone19.jpg'
					]
				},
				{
					'screenName' : 'Meditation Screen',
					'images' : [
						'helpScreens-iphone20.jpg',
						'helpScreens-iphone21.jpg',
						'helpScreens-iphone22.jpg',
						'helpScreens-iphone23.jpg'
					]
				}
			]
		},
		'ipad' : {
			'imagePath' : '/helpScreens/ios/ipad/',
			'items' : [
				{
					'screenName' : 'Home Screen',
					'images' : [
						'helpScreens-ipad1.jpg',
						'helpScreens-ipad2.jpg',
						'helpScreens-ipad3.jpg'
					]
				},
				{
					'screenName' : 'Meditation List',
					'images' : [
						'helpScreens-ipad4.jpg',
						'helpScreens-ipad5.jpg',
						'helpScreens-ipad6.jpg',
						'helpScreens-ipad7.jpg',
						'helpScreens-ipad8.jpg',
						'helpScreens-ipad9.jpg'
					]
				},
				{
					'screenName' : 'Meditation Settings',
					'images' : [
						'helpScreens-ipad10.jpg',
						'helpScreens-ipad11.jpg'
					]
				},
				{
					'screenName' : 'Interval Settings',
					'images' : [
						'helpScreens-ipad12.jpg',
						'helpScreens-ipad13.jpg',
						'helpScreens-ipad14.jpg'
					]
				},
				{
					'screenName' : 'Sounds',
					'images' : [
						'helpScreens-ipad15.jpg',
						'helpScreens-ipad16.jpg'
					]
				},
				{
					'screenName' : 'Animations',
					'images' : [
						'helpScreens-ipad17.jpg'
					]
				},
				{
					'screenName' : 'Meditation Length/Start Delay',
					'images' : [
						'helpScreens-ipad18.jpg',
						'helpScreens-ipad19.jpg'
					]
				},
				{
					'screenName' : 'Meditation Screen',
					'images' : [
						'helpScreens-ipad20.jpg',
						'helpScreens-ipad21.jpg',
						'helpScreens-ipad22.jpg'
					]
				}
			]

		},
	'support' : {
		'website' : 'http://gomeditate.code-lens.com/#contact',
		'email'	: 	'support@code-lens.com'
		}
	},
	'android' : {
		'imagePath' : '/helpScreens/android/',
		'items' : [],
	'support' : {
		'website' : 'http://code-lens.com',
		'email'	: 	'support@code-lens.com'
		}	
	}
};