var sounds = [
	{
		'soundID'	: 0,
		'soundName' : 'Silence',
		'soundFile' : ''
	},
	{
		'soundID'	: 1,
		'soundName' : 'Meditation Bell',
		'soundFile' : 'bell01.m4a'
	},
	{
		'soundID'	: 2,
		'soundName' : 'Deep Bell',
		'soundFile' : 'bell02.m4a'
	},
	{
		'soundID'	: 3,
		'soundName' : 'Temple Drum',
		'soundFile' : 'timpani_drum01.m4a'
	},
	{
		'soundID'	: 4,
		'soundName' : 'Kayagum High',
		'soundFile' : 'kayagum01.m4a'
	},
	{
		'soundID'	: 5,
		'soundName' : 'Kayagum Middle',
		'soundFile' : 'kayagum02.m4a'
	},
	{
		'soundID'	: 6,
		'soundName' : 'Kayagum Low',
		'soundFile' : 'kayagum03.m4a'
	},
	{
		'soundID'	: 7,
		'soundName' : 'Sōzu Single',
		'soundFile' : 'shishi_odoshi01.m4a'
	},
	{
		'soundID'	: 8,
		'soundName' : 'Sōzu Double',
		'soundFile' : 'shishi_odoshi03.m4a'
	},
	{
		'soundID'	: 9,
		'soundName' : 'Temple Bell 1x',
		'soundFile' : 'temple_bell_single.m4a'
	},
	{
		'soundID'	: 10,
		'soundName' : 'Temple Bell 2x',
		'soundFile' : 'temple_bell_double.m4a'
	},
	{
		'soundID'	: 11,
		'soundName' : 'Temple Bell 3x',
		'soundFile' : 'tepmple_bell_triple.m4a'
	},
	{
		'soundID'	: 12,
		'soundName' : 'Xylophone',
		'soundFile' : 'xylo01.m4a'
	}
];

var videos = [
	{
		'videoID' : 1,
		'videoName' : 'Red Candles',
		'videoThumbnail' : 'thumb_red_1.jpg',
		'videoFile' : 'red_candles.mp4'
	},
	{
		'videoID' : 2,
		'videoName' : 'Cinnamon Candle',
		'videoThumbnail' : 'thumb_cinnamon_1.jpg',
		'videoFile' : 'cinnamon_candle_1.mp4'
	},
	{
		'videoID' : 3,
		'videoName' : 'Large Candle',
		'videoThumbnail' : 'thumb_large_2.jpg',
		'videoFile' : 'large_candle_2.mp4'
	},
	{
		'videoID' : 4,
		'videoName' : 'Old Candle',
		'videoThumbnail' : 'thumb_old_1.jpg',
		'videoFile' : 'old_candle_1.mp4'
	},
	{
		'videoID' : 5,
		'videoName' : 'Round Candle',
		'videoThumbnail' : 'thumb_round_1.jpg',
		'videoFile' : 'round_candle_1.mp4'
	},
	{
		'videoID' : 6,
		'videoName' : 'Tealights',
		'videoThumbnail' : 'thumb_tealights_1.jpg',
		'videoFile' : 'tealights_1.mp4'
	},
	{
		'videoID' : 7,
		'videoName' : 'White Point',
		'videoThumbnail' : 'thumb_static_1.jpg',
		'videoFile' : 'static_1.mp4'
	},
	{
		'videoID' : 8,
		'videoName' : 'Blank Screen',
		'videoThumbnail' : 'thumb_static_2.jpg',
		'videoFile' : 'static_2.mp4'
	}
];

exports.sounds = sounds;
exports.videos = videos;