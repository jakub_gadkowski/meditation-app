/**
 * Meditation App default settings
 * 
 * @type {Object}
 */
exports.defaults = {
		//newly created meditation defaults
		meditation : {
        	name:'Custom meditation',
	        animation : 1, //id; default: 1st animation on the list
	        time : 600000, //miliseconds; default: 10 min
	        interval_type : 3, //Repeated Every/From Start/Before End/Off; default: Off
	        interval_time : 300000, //miliseconds; default: 5 min
	        interval_sound : 1, //sound id; default: 1st sound on the list 
	        start_sound : 1, //sound id; default: 1st sound on the list
	        end_sound : 2, //sound id; default: 2nd sound on the list
	        headstart : 10000, //miliseconds; default: 10 sec
	        type: 3	//default/builtin/custom/new custom ** probably obsolete **
		},
		app : {
			// tutorial
		}
};