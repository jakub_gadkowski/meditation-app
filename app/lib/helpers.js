/*************************
**         Init         **
**************************/
var filesInfo = require('filesInfo');


function msToTime (duration) {
    //var milliseconds = parseInt((duration%1000)/100);
    var seconds = parseInt((duration/1000)%60);
    var minutes = parseInt((duration/(1000*60))%60);
    var hours = parseInt((duration/(1000*60*60))%24);
    var result = '';

    if (hours > 0) {
        hours = (hours < 10) ? ' ' + hours + 'h ' : hours + 'h ';
        result += hours;
    }
    if (minutes > 0) {
        minutes = (minutes < 10) ? ' ' + minutes +' min ' : minutes + ' min ';
        result += minutes;
    }
    if (seconds > 0) {
        seconds = (seconds < 10) ? ' ' + seconds + ' s ' : seconds + ' s ';
        result += seconds;
    }

    return result; // hours + 'h ' + minutes +'min';// + ":" + seconds + "." + milliseconds;
}

function intervalLabel (intType) {
    var label = '';
    //determine label for Interval Type
    switch(intType) {
        case 0:
            label = 'Repeated Every';
            break;
        case 1:
            label = 'From Start';
            break;
        case 2:
            label = 'Before End';
            break;
        case 3:
            label = 'Off';
            break;
        default:
            label = 'Off';
            break;
    }
    return label;
}

function videoName (videoID) {
    var videos = filesInfo.videos; //Alloy.Globals.filesInfo.videos;
    return videos[videoID - 1].videoName;
}

function animateButton (el, args) {
    args = args || {};
    var params = {
        backgroundColor: '#fff',
        duration: 250,
        autoreverse: true
    };

    params.backgroundColor = args.backgroundColor || params.backgroundColor;
    params.duration = args.duration || params.duration;
    params.autoreverse = args.autoreverse || params.autoreverse;

    el.animate(params);
}

/*************************
**       Sounds         **
**************************/

var soundManager = {
    soundPlayer : Ti.Media.createSound(),
    /**
     * Play selected sound. If sound is already playing, stop it and then start new sound.
     * @method playSound
     * @param  {integer} soundID
     * @callback {Object} callback.finished - executes on sound 'complete'. Returns {void}.
     * @return {void}
     */
    playSound: function (soundID, callback) {

        var _this = this;
        callback = callback || undefined;
        //stop player if there is already sound playing
        if (this.soundPlayer.isPlaying()) this.soundPlayer.stop();

        soundID = soundID || 0;

        // If no soundID just play sound of silence :)
        if (soundID !== 0) {
            var loadedSound = this.retrieveSound(soundID);
            this.soundPlayer = Ti.Media.createSound({url:'/sound/'+loadedSound.soundFile});

            if (callback !== undefined && typeof callback.finished === 'function') {

                var listenerCallback = function() {
                    callback.finished.call(_this);
                    _this.soundPlayer.removeEventListener('complete', listenerCallback);
                };

                this.soundPlayer.addEventListener('complete', listenerCallback);
            }

            this.soundPlayer.play();
        } else if (soundID === 0 && ( callback !== undefined && typeof callback.finished === 'function')){
            callback.finished();
        }



    },
    resetSound: function() {
        var sP = this.soundPlayer;
        if (sP.isPlaying()) sP.stop();
        sP.release();
    },
    soundName: function(soundID) {
        var sounds = filesInfo.sounds;
        return sounds[soundID].soundName;
    },
    /**
     * Retrieve sound from the /lib/filesInfo and return the sound or empty object
     * @method retrieveSound
     * @param  {integer} soundID
     * @return {Object} - sound object
     */
    retrieveSound: function(soundID) {
        //if (soundID === 0) return {};

        var soundObject = filesInfo.sounds[soundID];
        return soundObject;
    }
};

/**
 * Proxy to provide 'this' to soundManager, otherwise it is this = helper.js
 * @param  {integer} soundID
 * @callback {Object} callback.finished - executes on sound 'complete'. Returns {void}.
 * @return {void}
 */
function soundManagerProx (soundID, callback) {
    soundManager.playSound.call(soundManager, soundID, callback);
}
/**
 * Proxy to provide 'this' to soundManager, otherwise it is this = helper.js
 * @return {void}
 */
function soundResetProx () {
    soundManager.resetSound.call(soundManager);
}

exports.playSound = soundManagerProx;
exports.videoName = videoName;
exports.soundName = soundManager.soundName;
exports.resetSound = soundResetProx;
exports.intervalLabel = intervalLabel;
exports.msToTime = msToTime;
exports.animateButton = animateButton;