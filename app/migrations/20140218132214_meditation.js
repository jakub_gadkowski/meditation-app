var preload_data = [
            {
                name : "Default 5 minutes",
                animation : 1,
                time : 300000,
                interval_type : 3,
                interval_time : 60000,
                interval_sound : 1,
                start_sound : 1,
                end_sound : 1,
                headstart : 10000,
                type: 1
            },
            {
                name : "Default 10 minutes ",
                animation : 1,
                time : 600000,
                interval_type : 2,
                interval_time : 300000,
                interval_sound : 3,
                start_sound : 1,
                end_sound : 1,
                headstart : 10000,
                type: 1
            },
            {
                name : "Default 30 minutes",
                animation : 1,
                time : 1800000,
                interval_type : 0,
                interval_time : 600000,
                interval_sound : 3,
                start_sound : 1,
                end_sound : 1,
                headstart : 20000,
                type: 1
            }
];

migration.up = function(migrator)
{
    migrator.createTable({
        "columns":
        {
            "meditation_id": "INTEGER PRIMARY KEY AUTOINCREMENT",
            "name" : "TEXT",
            "animation" : "INTEGER", //id
            "time" : "INTEGER", //miliseconds
            "interval_type" : "INTEGER", //Repeated Every/From Start/Before End,
            "interval_time" : "INTEGER", //miliseconds,
            "interval_sound" : "INTEGER", //sound id,
            "start_sound" : "INTEGER", //sound id,
            "end_sound" : "INTEGER", //sound id,
            "headstart" : "INTEGER", //miliseconds
            "type": "INTEGER" //default/builtin/custom/new custom
        }
    });
    for (var i = 0; i < preload_data.length; i++) {
        migrator.insertRow(preload_data[i]);
    }
};

migration.down = function(migrator)
{
    migrator.dropTable();
};