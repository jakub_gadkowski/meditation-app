// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

//Set Most Recent Meditation object ID
if (Ti.App.Properties.hasProperty('mostRecent') === false) {
	Ti.App.Properties.setInt('mostRecent',1);
}

//Set App version in iOS Settings
Ti.App.Properties.setString('version_preference', Ti.App.version+' (build: '+Ti.App.Properties.getInt('build')+')');