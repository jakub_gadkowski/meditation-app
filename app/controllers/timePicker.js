/*************************
**         Init         **
**************************/
var args = arguments[0] || {};
var self = $.timePickerWindow;

//Create appropriate time picker based on target that was used to invoke timePicker
if (args.targetID === 'meditHeadstart' || args.targetID === 'labelHeadstart') {
    $.addClass($.timePickerControls, 'timePickerSeconds');
    self.title = 'Start Delay';
    $.meditationLenght.text = 'Start Delay';
} else {
    $.addClass($.timePickerControls, 'timePickerCountDown');
    $.labelSeconds.setVisible(false);
    self.title = 'Meditation Lenght';
    $.meditationLenght.text = 'Meditation Lenght';
}

/*************************
**    Event Listeners   **
**************************/
var closeHandler = function() {
    self.removeEventListener('open', preselectTime);
    $.timePickerControls.removeEventListener('change', changeTime);
    self.removeEventListener('close', closeHandler);
    $.destroy();
};

//Perform tasks on screen closing
self.addEventListener('close', closeHandler);

self.addEventListener('open', preselectTime);
$.timePickerControls.addEventListener('change', changeTime);

/*************************
**    Interface build   **
**************************/

/**
* Toggle 'sec' label off & on
* @param {integer} toggle - '-1' toggles OFF, else toggles ON
* @returns void
*/
function toggleOff (toggle) {
    if (toggle === '-1') {
        $.labelSeconds.setVisible(false);
    } else {
        $.labelSeconds.setVisible(true);
    }
}

/**
* Preselects time in Time Picker on Controller Open
* @returns void
*/
function preselectTime () {
    //Time Picker set to 'Custom' type
    if ($.timePickerControls.type === -1) {
        //Painfuly iterate through all custom rows and find match to preselect
        $.headStartDelay.rows.forEach(function(el, i) {
            if (parseInt(el.ms) === args.model.get('headstart')) {
                $.timePickerControls.setSelectedRow( 0, i, false );
            }
        });
    //Time picker set to 'Countdown'
    } else if ($.timePickerControls.type === 3) {
        //Easy peasy - just set countDownDuration
        $.timePickerControls.countDownDuration = args.model.get('time');
    }
}

/*************************
**        Model         **
**************************/

/**
* Update model based on user selection from Time Picker, then update parent's interface
* @param {Object} e - Change Event from Time Picker
* @returns void
*/
function changeTime (e) {
    if (args.targetID === 'meditHeadstart' || args.targetID === 'labelHeadstart'){
        toggleOff(e.row.ms);
        //Update model
        args.model.set('headstart',parseInt(e.row.ms));

    } else {
        args.model.set('time',e.countDownDuration);

        //compare Interval & time, if time < interval, set interval to OFF
        if (e.countDownDuration <= args.model.get('interval_time')) args.model.set('interval_type', 3);
    }
    //Save model
    args.model.save();
    //Update parent's interface
    args.parent.updateInterfaceFromChild(args.model);
}