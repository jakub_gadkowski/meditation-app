/*************************
**         Init         **
**************************/
var settings = require('defaults');
var hlp = require('helpers');
var self = $.meditationWindow;
var args = arguments[0] || {};

//Array to hold 'meditaionRow' controllers
var data = [];
//Create collection with Meditation objects or fetch one if it already exists
var collection = Alloy.createCollection('meditation');//Alloy.Globals.Collection;

/**
* Set 'selected' icon on the selected table row and remove the 'selected'
* icon from the previously selected table row (if any). Then set the persiting setting 'mostRecent'
* to be equal to the selected meditation ID
*
* @param {Object} selectedObj - Click event target (should be tableViewRow or the parent of the Label that was touched)
* @method behindTheScene
* @method onTheScene
* @returns {void|tableViewRow} - Method behindTheScene returns adjusted object, method onTheScene adjust UI directly
*/
var setSelectedMeditation = {
    //Set mark icon visible on the row and return the row for Controller to build table
    behindTheScene : function(row) {
        row.children[0].setOpacity('1.0');
        // lastClicked = row;
        return row;
    },
    //Adjust visibility of the 'mark icon' on the table that is visible
    onTheScene: function(clicked) {
        var hide = {opacity: 0.0, duration: 250};
        var show = {opacity: 1.0, duration: 500};

        this._setMostRecent(clicked.rowID);

        data.forEach(function(el) {
            (el.rowID === clicked.rowID ? el.children[0].animate(show):el.children[0].animate(hide));
        });
    },
    _setMostRecent : function(id) {
        //set global, presistent 'mostRecent' meditation ID
        Ti.App.Properties.setInt('mostRecent',id);

        var model = collection.get(id);
        //Update interface of Home Screen with just selected meditation
        args.parent.updateInterfaceFromChild(model);
    }
};

/*************************
**    Event Listeners   **
**************************/

//Perform tasks on screen closing

var closeHandler = function() {
    //force refresh on Home Screen (in case any changes to the model were made)
    args.parent.updateInterfaceFromChild();

    $.medList.removeEventListener('delete', handleRowDelete);
    $.addButton.removeEventListener('click', addNewMeditation);
    $.medList.removeEventListener('click', handleTableClicks);
    self.removeEventListener('open', openHandler);
    self.removeEventListener('close', closeHandler);
    data = null;
    collection = null;
    self = null;

    //destroy model to prevent memory leaks
    $.destroy();
};

var openHandler = function() {
    //Preselecting mostRecentMeditation is happening in listSuccess loop
    initTable();
};

self.addEventListener('close', closeHandler);

self.addEventListener('open', openHandler);

//Delete 'swiped' row from DB
$.medList.addEventListener('delete', handleRowDelete);

//Add new meditation Row
$.addButton.addEventListener('click', addNewMeditation);

//Add event listener to tableRowView
$.medList.addEventListener('click', handleTableClicks);

/**
 * Handle 'Click' event on TableViewRow.
 * If clicked on "more info" icon opens Edit Meditation controller
 * If click was registered anywhere else - select that row and show selection icon.
 * @param  {Object} e - Click event object
 * @return {void}
 */
function handleTableClicks (e) {
    var clickedRow = e.row;
    var clickedID = e.source.id;

    if (clickedID === 'lblOptions') {
        //touch anim before transistion to new controller
        hlp.animateButton(clickedRow.children[3]);
        openMeditationDetails(clickedRow);
    }else{
        setSelectedMeditation.onTheScene(clickedRow);
    }
}

/*************************
**  Interface actions   **
**************************/

/**
* Parse fetched collection and for each result row create new controller with tableRow
* put all the tableRows into an array and attach it to the TableRowView
*
* @param {Object} collection - Fetched Backbone Collection object
* @returns {void}
*/
function listSuccess (collection) {
    data = [];
    var preselectedID = Ti.App.Properties.getInt('mostRecent');
    var row;
    //Go through fetched collection
    for (var i = 0, l = collection.length; i < l; i++){
        //retreive single model
        var item = collection.models[i];
        //create payload for TableViewRow Controller
        var payload = {
            rowID: item.attributes.meditation_id,
            itemName: item.attributes.name,
            model: item.attributes
        };
        //create table row controller and send the payload to it
        row = Alloy.createController('meditationRow', payload).getView();

        //Preselect selected row before the tableView is put on the scene
        if (preselectedID === payload.rowID) {
             row = setSelectedMeditation.behindTheScene(row);
        }
        // If only one row make it non-editable
        if (l === 1) row.editable = false;

        //add created table row to the array
        data.push(row);
    }
    //Add a list of table rows to the tableRowView
    $.medList.setData(data);
    row = null;
}

/**
* Add new meditation to the DB and new row to the table
* @returns {void}
*/
function addNewMeditation () {
    var defaults = settings.defaults.meditation;
    //TODO: move defaults to helper file or init
    var newMeditation = Alloy.createModel('meditation');
    newMeditation.set({
        //meditation defaults
        name: defaults.name +' '+ _.size(collection),
        animation : defaults.animation,
        time : defaults.time,
        interval_type : defaults.interval_type,
        interval_time : defaults.interval_time,
        interval_sound : defaults.interval_sound,
        start_sound : defaults.start_sound,
        end_sound : defaults.end_sound,
        headstart : defaults.headstart,
        type: defaults.type
    });

    //Save new meditation to the DB with default values
    newMeditation.save({},{
        success: function(){
            collection.add(newMeditation);

            var item = collection.models[collection.length - 1];
            // create payload for TableViewRow Controller
            var payload = {
                rowID: item.attributes.meditation_id,
                itemName: item.attributes.name,
                model: item.attributes
            };

            var row = Alloy.createController('meditationRow', payload).getView();
            data.push(row);
            $.medList.appendRow(row);
            row = null;
            //turn on 'swipe to Delete' on first row if more than 1 row
            if (collection.length >= 2) toggleDelete('on', data[0]);
        }
    });
}

/**
 * Handle row 'delete' action. Deletes model from collection, row from data Array,
 * toggles 'swipe to Delete' if less than 2 rows, and if deleted row was selected, selects next/previous row.
 * @param  {Object} e - target of delete event
 * @return {void}
 */
function handleRowDelete (e) {
    var modelToDelete = collection.get(e.row.rowID);
    var preselectedID = Ti.App.Properties.getInt('mostRecent');

    collection.remove(modelToDelete);

    //Adjust selected meditation to the previous one or the next one
    if (e.row.rowID === preselectedID){
        data.forEach(function (el,index) {

            if (e.row.rowID === el.rowID) {
                var newIndex = (index === 0) ? index + 1 : index - 1;
                var newItem = data[newIndex];
                setSelectedMeditation.onTheScene(newItem);
                return;
            }
        });
    }

    //remove swiped row from the data Array
    data.forEach(function (el,index) {
        if (e.row.rowID === el.rowID) {
            data.splice(index, 1);
            return;
        }
    });

    // if there is only one left turn off 'swipe to delete'
    if (collection.length === 1) {
        toggleDelete('off', data[0]);
    }

    //remove 'swiped' row from DB
    modelToDelete.destroy();
}

/**
 * Toggle 'swipe to delete' on given TableViewRow
 * @param  {string} toggle - 'on'/'off'
 * @param  {Object} el - TableViewRow object to be toggled.
 * @return {void}
 */
function toggleDelete (toggle, el) {
    (toggle === 'on' ? el.editable = true : el.editable = false);
}

/**
* Proxy function that makes updating interface of this controller from any child controller easier
* @param {Object|null} model - Model object, or null.
* @returns void
*/
function updateInterfaceFromChild () {

    initTable();
}


/*************************
**Controllers interaction**
**************************/

/**
* Opens 'Edit Meditation' navigation window in NavigationWindow stack.
* Passes the info about the meditation to the newly opened Window.
*
* @param {Object} selectedObj - Should be the parent tableRow of the touched 'more' label.
* @returns {void} - Opens new navigation window stack
*/
function openMeditationDetails (selectedObj) {
    var rowID = selectedObj.rowID;
    var nav = Alloy.Globals.navMenu;
    var requestedController = 'meditationEdit';
    var payload = {
            parent : $,
            meditationID: rowID
        };

    if (requestedController !== '') {
        //Opening new window in Navigation Windows stack
        if(OS_IOS){
            nav.openWindow(Alloy.createController(requestedController, payload).getView());
        }else{
            //on all others just open a new window
            Alloy.createController(requestedController).show();
        }
    }
}

/*************************
**    Open Controller   **
**************************/

/**
 * Load collection on meditation models and on success initialise meditation table
 * @return {void}
 */
function initTable () {
    //Fetch collection of Meditations from the DB
    collection.fetch({
    success: listSuccess,
    //TODO: add general error handling
    error: function(){
                Ti.API.error('collection fetch failed');
            }
    });
}



/*************************
**        Exports       **
**************************/
exports.updateInterfaceFromChild = updateInterfaceFromChild;

if(OS_ANDROID) {
//For android not used on iOS
    exports.show = function() {
	    self.open({
            activityEnterAnimation: Ti.Android.R.anim.fade_in,
            activityExitAnimation: Ti.Android.R.anim.slide_out_right
	    });
    };
}