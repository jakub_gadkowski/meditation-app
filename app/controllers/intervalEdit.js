/*************************
**         Init         **
**************************/
var hlp = require('helpers');
var args = arguments[0] || {};
var self = $.intervalEditWindow;

/*************************
**    Event Listeners   **
**************************/
var openHandler = function() {
    prepareIntervalEditView();
};

var closeHandler = function() {
    updateModel();
    // args.model.save();
    // args.parent.updateInterfaceFromChild(args.model);
    $.timePickerControls.removeEventListener('change', updateInterval);
    $.intervalOptions.removeEventListener('click', toggleIntervalInterface);
    $.selectIntervalSound.removeEventListener('click', openNavSubView);
    self.removeEventListener('open', openHandler);
    self.removeEventListener('close', closeHandler);
    //destroy model to prevent memory leaks
    $.destroy();
};

self.addEventListener('open', openHandler);

self.addEventListener('close', closeHandler);

$.timePickerControls.addEventListener('change', updateInterval);
$.intervalOptions.addEventListener('click', toggleIntervalInterface);
$.selectIntervalSound.addEventListener('click', openNavSubView);



/*************************
**    Interface build   **
**************************/

function updateInterfaceFromChild (model) {
     $.soundName.text = hlp.soundName(model.get('interval_sound'));
}

/**
* Prepopulate Edit Interval screen from model passed in from parent
* @returns void
*/
function prepareIntervalEditView () {
    var time = args.model.get('time');
    //Hide interface if 'interval_type' is 3 (Off)
    if (args.model.get('interval_type') === 3) {
        var selectedOff = {
            'index' : 3
        };
        toggleIntervalInterface(selectedOff);
    }

    // If time is set to 1 minute, turn off Interval completely
    if (time === 60000) {
        var labelsArr = $.intervalOptions.labels;
        labelsArr.forEach(function(el) {
            el.enabled = false;
        });

        $.intervalOptions.setLabels(labelsArr);
    }

    //populate interface from model
    // $.intervalTime.setText(hlp.msToTime(args.model.get('interval_time')));
    $.soundName.text = hlp.soundName(args.model.get('interval_sound'));

    $.meditationTime.text = hlp.msToTime(time);
    $.intervalOptions.index = args.model.get('interval_type');
    $.timePickerControls.countDownDuration = args.model.get('interval_time');


}

/**
* Update Interval time on screen as well as in model, based on user interaction with Time Picker
* @param {Object} e - Time Picker 'change' event
* @returns void
*/
function updateInterval (e) {
    //if selected interval time is more than meditation time - change it to (meditation time - 1min).
	if (e.countDownDuration >= args.model.get('time')) {
		$.timePickerControls.setCountDownDuration(args.model.get('time') - 60000);
	}
    // Update interface
	// $.intervalTime.setText(hlp.msToTime($.timePickerControls.countDownDuration));
    // Update model
    // args.model.set('interval_time', $.timePickerControls.countDownDuration);
    updateModel('interval_time', $.timePickerControls.countDownDuration);
}

/**
* Turn Interval Edit interface ON & OFF
* @param {Object} e - TabbedBar 'click' event
* @returns void
*/
function toggleIntervalInterface (e) {

    if (e.index !== 3) {
        // $.intervalTime.setVisible(true);
        $.timeControls.setVisible(true);
        $.selectIntervalSound.setVisible(true);
        $.toggleShadow.setVisible(true);
    //If TabbedBar 'OFF' option was selected, turn off interface
    } else {
        $.timeControls.setVisible(false);
        $.toggleShadow.setVisible(false);
        $.selectIntervalSound.setVisible(false);

    }

    updateModel('interval_type', e.index);
}


/*************************
**Controllers interaction**
**************************/

/**
* Opens sub navigation window in NavigationWindow stack.
* Passes the info about the meditation to the newly opened Window.
*
* @param {Object} event - 'Click' event target
* @returns {void} - Opens new navigation window stack
*/
function openNavSubView (event) {
    var nav = Alloy.Globals.navMenu;
    var requestedController = 'soundList';
    var payload = {
            parent: $,
            model: args.model,
            targetID: 'selectIntervalSound'
        };

    if(OS_IOS){
        nav.openWindow(Alloy.createController(requestedController, payload).getView());
    }else{
        //on all others just open a new window
        Alloy.createController(requestedController).show();
    }
}


/*************************
**         Model        **
**************************/

/**
* Update model, save it and update parent interface
* @param {string} parameter - Name of Model parameter to update
* @value {integer?} value - New value for parameter (optional). If omited the model will not be updated, just saved and parent will be updated.
* @returns void
*/
function updateModel (parameter, value) {

    if (value !== undefined && parameter) {
        args.model.set(parameter, value);
    }
    args.model.save();
    args.parent.updateInterfaceFromChild(args.model);
}

/*************************
**        Exports       **
**************************/
exports.updateInterfaceFromChild = updateInterfaceFromChild;