/*************************
**         Init         **
**************************/
var filesInfo = require('filesInfo');
var hlp = require('helpers');

var videos = filesInfo.videos;

var self = $.meditateWindow;
var args = arguments[0] || {};

var meditationTime = Number(args.model.get('time'));
var headstart = Number(args.model.get('headstart'));
var intervalType = args.model.get('interval_type');
var intervalTime = args.model.get('interval_time');
var intervalSound = args.model.get('interval_sound');
var startSound = args.model.get('start_sound');
var endSound = args.model.get('end_sound');

var phase = 0;
var tick = 1000; //ms
var videoOptions = {
    url : videoURL(),
    title : args.model.get('name')
};


/**
 * A Video Controls (Play/Pause/Stop) object. Defines behaviours for play/pause and stop buttons. Shows and hides controls.
 * @type {Object}
 */
var VideoControls = {
    /**
     * Default settings, if you provide 'config' to this.init matching lines will be merged
     * @type {dictonary}
     */
    config : {
        tick : 1000,
        controlsTimeOut : 5000,
        delta : 0,
        state : 'off',
        controlsTimerStart : Date.now(),
        controlsContainer : $.videoControls,
        playButton : $.playPauseButton,
        infoLabel : $.infoLabel,
        stopButton : $.stopButton,
        pauseScreen : $.pauseScreen,
        parent : $.meditateWindow,
        pauseState: 0
    },
    /**
     * setInterval identifier
     */
    videoControlsInterval : null,
    /**
     * Initialise VideoControls with optional configuration parameters.
     * @method init
     * @param  {dictionary?} config - Configuration that will override default settings.
     * If 'config' provided, property from 'config' will be merged with this.config overwritting matching this.config property.
     * @return {void}
     */
    init : function(config) {
        //initialise settings from this.config or use valuses passed by user
        this.extend(this.config, config);
        var c = this.config;

        this._attachListeners();
        //show or hide controls
        var state = c.state;
        var timeNow = Date.now();
        c.delta = (timeNow - c.controlsTimerStart) + c.tick;

        if (state === 'on') {
            this._showControls(c.delta);
        } else {
            this._hideControls();
        }
    },
    stopButtonHandler : function() {},
    playPauseButtonHandler : function() {},
    /**
     * Attaches listeners to play/pause/stop buttons
     * @method _attachListeners
     * @return {void}
     */
    _attachListeners : function() {
        var c = this.config;
        var _this = this;

        if (c.playButton.hasListener !== 1 ) {

            this.stopButtonHandler = function() {
                _this._togglePause();
                c.delta  = 1000;
            };

            c.playButton.addEventListener('click', this.stopButtonHandler);
            c.playButton.hasListener = 1;
        }

        if (c.stopButton.hasListener !== 1 ) {

            this.playPauseButtonHandler = function() {
                _this.killTimeout();
                c.parent.close();
            };
            c.stopButton.addEventListener('click', this.playPauseButtonHandler);
            c.stopButton.hasListener = 1;
        }
    },
    _removeListeners : function() {
        var c = this.config;
        c.playButton.removeEventListener('click', this.playPauseButtonHandler);
        c.stopButton.removeEventListener('click', this.stopButtonHandler);
    },
    /**
     * Show controls on screen and start timeout loop.
     * If timeout runs out (defult 5sec) kill loop and hide controls
     * @method _showControls
     * @param  {integer} delta - Time difference between now and 'controls visible' time.
     * @return {void}
     */
    _showControls : function(delta) {
        var c = this.config;
        var _this = this;

        //if meditationTime is available show it, if not show playing status
        if (meditationTime) {
            c.infoLabel.setText(hlp.msToTime(meditationTime));
        } else {
            //TODO: add showing playing status
            c.infoLabel.setText('Zzz...');
        }

        //show controls
        c.controlsContainer.setVisible(true);
        c.controlsContainer.animate({
            opacity: 1.0,
            duration: 500
        });

        //Start timout loop. After default time hide controls
        this.videoControlsInterval = setTimeout(function() {

            if (c.delta >= c.controlsTimeOut) {
                _this.killTimeout();
                _this._hideControls();
            } else {
                c.delta += c.tick;
                _this._showControls(delta);
            }
        }, c.tick);
    },
    /**
     * Hide controls with animation
     * @method
     * @return {void}
     */
    _hideControls : function() {
        var c = this.config;

        c.controlsContainer.animate({
            opacity: 0.0,
            duration: 500
        }, function() {
            c.controlsContainer.setVisible(false);
        });
    },
    /**
     * On 'click' emit event that will toggle Pause state On or Off
     * @method _togglePause
     * @fires #togglePause
     * @return {void}
     */
    _togglePause : function() {
        var c = this.config;

        // Toggle Pause meditation
        c.parent.fireEvent('togglePause',{event : {
                source : this.config.playButton
            }
        });

        this.togglePauseScreen();
    },
    /**
     * Switches 'Pause' screen on/off
     * @method togglePauseScreen
     * @return {void}
     */
    togglePauseScreen : function() {
        var c = this.config;

        if (c.pauseScreen.isOn === 1) {
            // Fade out pause screen
            c.pauseScreen.animate({
                opacity: 0.0,
                duration: 500
            }, function() {
                c.pauseScreen.setVisible(false);
            });

            c.pauseScreen.isOn = 0;
        } else {
            // Fade in pause screen
            c.pauseScreen.setVisible(true);
            c.pauseScreen.animate({
                opacity: 0.7,
                duration: 500
            });
            c.pauseScreen.isOn = 1;
        }
    },
    /**
     * Release VideoControls and free memory. This method should be used at window.close().
     * @method release
     * @return {void}
     */
    release :  function() {
        this._removeListeners();
        this.killTimeout();
        VideoControls = null;
    },
    /**
     * Destroy timout loop
     */
    killTimeout : function() {
        clearTimeout(this.videoControlsInterval);
    },
    /**
     * Helper method. Destructively merges properties with the same keys from 'extendee' into 'extending' object.
     * @method extend
     * @param  {Object} extending - Object that will have properties merged into it.
     * @param  {Object} extendee  - Object that will provide properties to merge.
     * @return {void}
     */
    extend : function(extending, extendee) {
        if (!extendee || typeof extendee !== 'object') return;

        for (var prop in extendee) {
            if (prop in extending) {
                extending[prop] = extendee[prop];
            }
        }
    }

};

/**
 * Main time loop. For head start and main meditation time
 * @type {Object}
 */
var mainLoop = {
    /**
     * Interval identifier
     */
    intervalID : null,
    interval : 500,
    togglePauseHolder : $.meditateWindow,
    init: function(interval) {

        if (interval) {
            this.interval = interval;
        } else {
            interval = this.interval;
        }

        this._addListeners();
        this.start(interval);
    },
    /**
     * Start loop
     * @param  {integer} interval - How often should the loop run. Default 1000ms.
     * @return {void}
     */
    start : function(interval) {
        var _this = this;
        //start loop
        this.intervalID = setInterval(function(){
            _this.checkPhase();
        }, interval);
    },
    togglePauseHandler: function() {},
    /**
     * Attach listeners
     */
    _addListeners : function() {
        var _this = this;
        this.togglePauseHandler = function() {
            _this.togglePause();
        };

        // attach listener to the main window to listen to the 'togglePause' event.
        this.togglePauseHolder.addEventListener('togglePause', this.togglePauseHandler);
    },
    /**
     * Remove attached listeners
     * @method _removeListeners
     * @return {void}
     */
    _removeListeners : function() {
        this.togglePauseHolder.removeEventListener('togglePause', this.togglePauseHandler);
    },
    release: function() {
        this.killInterval();
        this._removeListeners();
        mainLoop = null;
    },
    togglePause : function() {
        if (this.intervalID !== null) {
            //Pause loop
            this.killInterval();
        } else {
            // Start Main Loop
            this.start(this.interval);
        }
    },
    killInterval : function() {
        clearInterval(this.intervalID);
        this.intervalID = null;
    },
    /**
     * Check for phase of meditation and fire headStart or normal timer.
     * @return {void}
     */
    checkPhase : function() {
        //if is headstart and headstart is not 'OFF'
        if (phase === 1 && headstart !== -1) {
            headStart();
        //else headstart is over, or headstart is 'OFF'
        } else if (phase === 2 || headstart === -1) {
            timer();
        }
    }
};

/*************************
**    Event Listeners   **
**************************/

if (ENV_DEV) {
    $.headStartDialog.addEventListener('click', function() {
        self.close();
    });
}


/*************************
**         Main         **
**************************/

var closeHandler = function() {
    // Release all prisoners!
    videoPlay.release();
    VideoControls.release();
    if (mainLoop) mainLoop.release();

    hlp.resetSound();
    self.removeEventListener('close', closeHandler);
    $.destroy();
};

/**
 * Initialise screen on open()
 *
 */
function bootStrap () {
    //Create video container
    // vid = videoPlay.init(videoOptions);
    //Add video container to screen
    self.add(videoPlay.init(videoOptions));
    //default phase 1: headstart
    phase = 1;

    headstartLabel();
    //start main loop with default interval
    mainLoop.init(tick);

    // Add event listener for 'close' and clear the scene
    self.addEventListener('close', closeHandler);
}

/**
 * Head Start loop. Shows headstart dialog and countdown.
 * On Headstart reaching changes phase to 2 (time loop)
 * @return {void}
 */
function headStart () {
    if (headstart > 0) {
        //show countdown
        $.headStartDialog.text = hlp.msToTime(headstart);
        headstart = headstart - tick;
    } else {
        headstartLabel('off');
        //phase 2 - meditation time
        phase = 2;
    }

}

/**
 * Hides or shows the Headstart countdown label, also checks if Headstart is turned on
 * @param  {string?} toggle - Optional 'on' to show label, 'off' to hide it.
 * By default checks the Headstart status and if status not '-1' turns label on.
 * @return {void}
 */
function headstartLabel (toggle) {
    toggle = toggle || 'on';

    if (headstart !== -1 && toggle === 'on') {
        $.headStartWrap.visible = true;
        $.headStartWrap.animate({
                opacity: 1.0,
                duration: 500
            });
        return;
    }

    if (toggle === 'off') {
        $.headStartWrap.animate({
                opacity: 0.0,
                duration: 500
            }, function(){
                $.headStartWrap.visible = false;
            });
    }
}

/**
 * Main time loop. Plays sound on time start, and time end. Also checks for Intervals
 * On time end closes meditation screen.
 * @return {void}
 */
function timer () {
    //Play sound at the start of meditation
    if (meditationTime === args.model.get('time')){
        hlp.playSound(startSound);
    }
    //Meditation time more than 0
    if ( meditationTime > 0) {

        //if Interval is not Off in settings
        if (intervalType !== 3) {
            checkInterval();
        }

        meditationTime = meditationTime - tick;
    //Meditation time less than/equal to zero
    } else {
        // stop main loop
        mainLoop.release();
        // Play End sound, and when sound stops playing close window
        hlp.playSound(endSound, {
            finished: function(){
                // fadeout and close meditation screen
                self.animate({
                    opacity : 0.0,
                    backgroundColor : 'white',
                    duration : 500
                }, function(){
                    self.close();
                });
            }
        });
    }
}

/**
 * Checks Interval and if Interval fulfiled play interval sound
 * @return {void}
 */
function checkInterval () {

    var expectations = false;
    var originalTime = args.model.get('time');
    var calc;

    //Determine expectations
    //If Interval is set to 'Repeat' skip the first interval
    //as it will interfere with Start sound
    if (intervalType === 0 && meditationTime !== originalTime){
        expectations = (meditationTime % intervalTime ? false : true);
    //Interval is set to From Start
    } else if (intervalType === 1) {
        calc = originalTime - intervalTime;
        expectations = (calc === meditationTime ? true : false);
    //Interval is set to Before End
    } else if (intervalType === 2) {
        expectations = (intervalTime === meditationTime ? true : false);
    }

    //If expectations returns 'true' play sound
    if (expectations === true) {
        hlp.playSound(intervalSound);
    }
}

/**
 * Get video path based on videoID
 * @return {string} Video path.
 */
function videoURL () {
    var staticPath = '/video/';
    var videoID = args.model.get('animation');
    var videoPath = videos[videoID - 1].videoFile;

    return staticPath+videoPath;
}

/**
 * Video Player. Creates Titanium.Media.VideoPlayer, adds listeners to it, and initialises VideoControls
 *
 * @return {void} Titanium.Media.VideoPlayer
 */
var videoPlay = {
    defaults : {
        backgroundColor : '#000',
        url : null,
        fullscreen : false,
        autoplay : true,
        scalingMode : Ti.Media.VIDEO_SCALING_ASPECT_FIT,
        repeatMode: Ti.Media.VIDEO_REPEAT_MODE_NONE,
        mediaControlStyle : Ti.Media.VIDEO_CONTROL_NONE
    },
    video:{},
    /**
     * Initialise videoPlayer
     * @method init
     * @param  {Dictonary} _args - settings for Titanium.Media.VideoPlayer. If not supplied, defaults will be used.
     * @return {Object} Titanium.Media.VideoPlayer
     */
    init : function(_args) {
        var videoInit = {
            backgroundColor : _args.backgroundColor || this.defaults.backgroundColor,
            url : _args.url || this.defaults.url,
            fullscreen : _args.fullscreen || this.defaults.fullscreen,
            autoplay : _args.autoplay || this.defaults.autoplay,
            scalingMode : _args.scalingMode || this.defaults.scalingMode,
            // repeatMode: _args.repeatMode || Ti.Media.VIDEO_REPEAT_MODE_ONE,
            mediaControlStyle : _args.mediaControlStyle || this.defaults.mediaControlStyle
        };

        this.video = Ti.Media.createVideoPlayer(videoInit);

        this.addListeners(this.video);

        return this.video;
    },
    /**
     * Closes VideoPlay and releases resources
     * @method release
     * @return {void}
     */
    release : function() {
        this.removeListeners(this.video);
        this.video.stop();
        videoPlay = null;
    },
    playbackStateHandler : function() {},
    clickHandler : function() {},
    /**
     * Attach listeners to the video object. Listeners attached: playbackstate; click.
     * @method setListeners
     * @param {Object} obj - Titanium.Media.VideoPlayer
     */
    addListeners: function(obj) {

        this.playbackStateHandler = function(e) {

            if (e.playbackState === 2) {
                obj.play();
             }
        };
        // Restart on stop (workaround for iphone 4 freezing play on loop)
        obj.addEventListener('playbackstate', this.playbackStateHandler);

        this.clickHandler = function() {
            var settings = {
                controlsTimerStart : Date.now(),
                state : 'on'
            };
            VideoControls.init(settings);
        };
        // Attach Video Controls and show them on 'Click'
        obj.addEventListener('click', this.clickHandler);

    },
    /**
     * Removes all attached listeners from VideoPlayer object
     * @method removeListeners
     * @param {Object} obj - Titanium.Media.VideoPlayer
     * @return {void}
     */
    removeListeners: function(obj) {
        obj.removeEventListener('click', this.clickHandler);
        obj.removeEventListener('playbackstate', this.playbackStateHandler);

       if (ENV_TEST || ENV_DEV){
            //body
       }
    }

};

/*************************
**        Exports       **
**************************/
exports.show = function() {

    bootStrap();
    self.open({
         // activityEnterAnimation: Ti.Android.R.anim.fade_in,
         // activityExitAnimation: Ti.Android.R.anim.slide_out_right
    });
};