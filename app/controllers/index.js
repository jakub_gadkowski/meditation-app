/*************************
**         Init         **
**************************/
var hlp = require('helpers');

var self = $.navigationWin;
//set a global reference to the navmenu that's needed for NavigationWindow stack
Alloy.Globals.navMenu = self;

var mostRecentID = Ti.App.Properties.getInt('mostRecent');
var mostRecentModel = Alloy.createModel('meditation');

/*************************
**    Event Listeners   **
**************************/
self.addEventListener('open', function() {
    updateInterfaceFromChild();
});

/*************************
**    Interface build   **
**************************/

/**
* Proxy function that makes updating interface of this controller from any child controller easier
* @param {Object|null} model - Model object, or null.
* @returns void
*/
function updateInterfaceFromChild () {
    //get Model id of the most recently chosen meditation
    mostRecentID = Ti.App.Properties.getInt('mostRecent');

    //Fetch this model freshly from DB
    mostRecentModel.fetch({
        id: mostRecentID,
        success: function() {
            //do the interface update using the fresh model
            updateHomeUI(mostRecentModel);
        },
        error: function() {
            Ti.API.error('collection failed');
        }
    });
}

/**
* Update Home screen interface with fresh data from Model
* @param {Object} model - Model object.
* @returns void
*/
function updateHomeUI (model) {
    //Not sure if this is needed anymore,
    //as this function is supposed to be invoked always with model as param
    //var model = model || mostRecentModel;

    //Update interface elements
    $.medLabel.applyProperties({
        text : model.get('name')
    });
    $.videoName.applyProperties({
        text : hlp.videoName(model.get('animation'))
    });
    $.timeLabel.applyProperties({
        text : hlp.msToTime(model.get('time'))
    });
}


/*************************
**Controllers interaction**
**************************/

/**
* Create new controller and open it on top of NavigationWindow stack
* @param {Object} eventTarget - Click/touch target
* @returns void
*/
function openController (eventTarget) {
    var itemClicked = eventTarget.source;

    //create payload for the childController
    var payload = {
            //parent is needed to access/update view associated with this controller
            parent: $,
            model: mostRecentModel,
            //targetID needed for timePicker controller
            targetID: itemClicked.id
        };
    //retrieve controller name based on the item clicked/touched
    var requestedController = getController(itemClicked);


    if (requestedController !== '') {
        //Opening new window in NavigationWindow stack
        if(OS_IOS){
            self.openWindow(Alloy.createController(requestedController,payload).getView());
        }else{
            //on all others just open a new window
            Alloy.createController(requestedController).show();
        }
    }
}

/**
* Retrieve custom value from the clicked item and return the controller name.
* @param {Object} itemClicked
* @returns {string} Returns controller name as string
*/
function getController (itemClicked) {

    var controllerName;
    // check if controller name is defined on the object
    if(itemClicked.childController !== undefined) {
        controllerName = itemClicked.childController;
    }else{
        //controller name was not found,
        //get the parent of the object and check the parent
        var parent = itemClicked.getParent();

        controllerName = getController(parent);
        //we do not want to check parent more levels then one above
        return controllerName;
    }
    return controllerName;
}

/**
* Opens Meditation screen (VideoPlayer) in new window (both iOS and Android).
* @returns void
*/
function openMeditation (e) {

    hlp.animateButton(e.source, {backgroundColor: '#e93f4d'});

    var mostRecentID = Ti.App.Properties.getInt('mostRecent');

    var recentModel = Alloy.createModel('meditation');

    recentModel.fetch({
        id: mostRecentID,
        success: function() {

            var payload = {
                model : recentModel
            };
            Alloy.createController('meditate', payload).show();
        },
        error: function() {
            Ti.API.error('collection failed');
        }
    });
}

/*************************
**    Open Controller   **
**************************/
self.open();

/*************************
**        Exports       **
**************************/
exports.updateInterfaceFromChild = updateInterfaceFromChild;


//For Specs only
if (ENV_DEV) {

    exports.setLabel = function (id, text) {
        $[id].text = text;
    };

    exports.getController = getController;

    exports.test = function() {
        return 1;
    };
}