/*************************
**         Init         **
**************************/
var filesInfo = require('filesInfo');
var videos = filesInfo.videos;
var self = $.animationGallery;
var args = arguments[0] || {};


/*************************
**    Event Listeners   **
**************************/

var openHandler = function() {
    createGallery();
    preselectVideo();
};

var closeHandler = function() {
	$.galleryWrapper.removeEventListener('click', clickHandler);
	self.removeEventListener('open', openHandler);
	self.removeEventListener('close', closeHandler);
    //destroy model to prevent memory leaks
    $.destroy();
};

var clickHandler = function(e) {
	//On click deselect all thumbnails and set chosen thumbnail to selected
	checkSelected(e.source);
};

self.addEventListener('open', openHandler);
//Perform tasks on screen closing
self.addEventListener('close', closeHandler);
$.galleryWrapper.addEventListener('click', clickHandler);


/*************************
**    Interface build   **
**************************/

/**
* Create 'gallery' of video/static visualistions based on lib/filesList, then show thumbnails
* @returns {Void}
*/
function createGallery () {
	//Use videos from /lib/filesList
	videos.forEach(function(el) {
		var payload = {
			videoID : el.videoID,
			videoThumbnail: el.videoThumbnail
        };
        //Create controller with single gallery entry
	    var controller = Alloy.createController('animationGalleryItem', payload).getView();
	    //Add controller to the gallery View
		$.galleryWrapper.add(controller);
	});
}

/**
* Iterate through all thumbnails in the gallery and remove selection,
* then add selection to chosen thumbnail. After that invoke updateModel()
* @param {Object} selectedObj - UI element passed from the 'clicked' event
* @returns {void}
*/
function checkSelected (selectedObj) {
	var checkedElement = selectedObj || {};
	var elApiName = selectedObj.apiName;
	var selectedID = checkedElement.videoID || 0;
	//get list of thumbnails in the gallery
	var wrapperElements = $.galleryWrapper.getChildren();

	//assign value of checkedElement based on the UI element that was touched
	if (elApiName === 'Ti.UI.ImageView') {
		//clear visibility from all 'checked marks'
		wrapperElements.forEach(function(el){
			// el.children[0].visible = false;
			if (el.isSelected === true && selectedID !== el.videoID) {
				el.animate({
					opacity: 1.0,
					duration : 250
				});
				el.isSelected = false;
			}else if (selectedID === el.videoID){
				el.animate({
					opacity: 0.7,
					duration : 250
				});
				el.isSelected = true;
			}
		});
		//Invoke modelUpdate and interface update in parent controller
		updateModel(args.model, selectedID);
	}
}

/**
* Preselect visualisation thumbnail based on the Meditation model settings from args.model
* @returns {void}
*/
function preselectVideo () {
	var selectedVideo = (args.model !== undefined) ? args.model.get('animation') : 1;

	var wrapperElements = $.galleryWrapper.children;

	wrapperElements.forEach(function(el) {
		if (el.videoID === selectedVideo) {
			checkSelected(el);
		}
	});
}


/*************************
**        Model         **
**************************/

/**
* Update 'animation' property of given model, and update parent interface with that model
* @param {Object} modelToUpdate - Backbone model to update
* @param {integer} videoID - Id of the affected video
* @returns {Void|bool} - Returns false if model is undefined
*/
function updateModel (modelToUpdate, videoID) {
	if (modelToUpdate === undefined) return false;

	modelToUpdate.set('animation', videoID);
	modelToUpdate.save();
	//update interface of the parent controller
	args.parent.updateInterfaceFromChild(modelToUpdate);
}


/*************************
**        Exports       **
**************************/
if (ENV_DEV) {
    //For Specs only
    exports.checkSelected = checkSelected;
    exports.preselectVideo = preselectVideo;
    exports.updateModel = updateModel;
    exports.createGallery = createGallery;
}