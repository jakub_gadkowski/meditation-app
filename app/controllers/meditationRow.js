/*************************
**         Init         **
**************************/
var hlp = require('helpers');
var args = arguments[0] || {};
var model = args.model;

/*************************
**    Open Controller   **
**************************/
//assign custom values to Labels and tableRow
$.listRow.rowID = args.model.meditation_id;
$.lblTitle.text = args.model.name;
$.lblInfoTime.text = hlp.msToTime(args.model.time);
$.lblInfoHeadStart.text = (args.model.headstart != -1) ? hlp.msToTime(args.model.headstart) : 'Off';
$.lblInfoAnim.text = hlp.videoName(args.model.animation);