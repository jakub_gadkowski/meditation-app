/*************************
**         Init         **
**************************/
var hlp = require('helpers');

var args = arguments[0] || {};
var self = $.meditationEditWindow;
var meditModel = Alloy.createModel('meditation');


/*************************
**    Event Listeners   **
**************************/



var closeHandler = function() {
    meditModel.save();
    // manageListeners('remove');
    $.meditationEditWindow.removeEventListener('click', clickManagement);
    $.meditName.removeEventListener('return', returnHandler);
    $.meditName.removeEventListener('focus', focusHandler);
    $.meditName.removeEventListener('blur', blurHandler);
    $.meditName.removeEventListener('close', closeHandler);
    //destroy model to prevent memory leaks
    $.meditationEditWindow = null;
    $.destroy();
};

var focusHandler = function() {
    $.meditName.hasFocus = true;
};

var blurHandler = function() {
    $.meditName.hasFocus = false;
};

var returnHandler = function() {
    meditModel.save({
        name: $.meditName.value
    },{
        success: function() {
            args.parent.updateInterfaceFromChild();
        }
    });
};
//Perform tasks on screen closing
self.addEventListener('close', closeHandler);

$.meditationEditWindow.addEventListener('click', clickManagement);
$.meditName.addEventListener('focus', focusHandler);
$.meditName.addEventListener('blur', blurHandler);
//Save Name on return, discard on blur.
$.meditName.addEventListener('return', returnHandler);


/*************************
**    Manage Listeners  **
**************************/

/*
* Manages click events. If TextField has focus then cancel edit on click, if not then open controller based on click target
* @param {Object} event - Click event.
* @returns {void|null} Returns null if click was registered on TextField.
*/
function clickManagement (event) {
    var clicked = event.source;
    //textfield clicked
    if (clicked.id === 'meditName' || clicked.id === 'editLabel') return;
    //cancel edit
    if ($.meditName.hasFocus === true){
        restoreEditedText();
        $.meditName.blur();
    } else {
        //open controller
        if(clicked.childController) openNavSubView(clicked.childController, clicked.id);
    }
}


/*************************
**    Interface build   **
**************************/

function updateInterfaceFromChild (model) {
    updateInterface(model);
    //Update Meditation List controller on change
    args.parent.updateInterfaceFromChild();
}

function restoreEditedText () {
    $.meditName.value = meditModel.get('name');
}


/**
* Updates information about meditation.
* @param {Object} model - updated meditation Model.
* @returns void
*/
function updateInterface (model) {
    var meditModel = model;

    //Meditation Name
    $.meditName.value = meditModel.get('name');
    //Meditaton Animation
    $.meditAnimation.text = hlp.videoName(meditModel.get('animation'));
    //Meditation lenght converted to minutes and hours
    $.meditTime.text = hlp.msToTime(meditModel.get('time'));
    //Meditation Interval info (type, time, sound)
    if (meditModel.get('interval_type') === 3) {

        $.resetClass($.meditInterval, 'item largeText');

        $.meditInterval.applyProperties({
            text : hlp.intervalLabel(meditModel.get('interval_type'))
        });
    } else {

        $.resetClass($.meditInterval, 'item smallText');

        var preparedLabel = hlp.intervalLabel(meditModel.get('interval_type'));
        preparedLabel += ': ' + hlp.msToTime(meditModel.get('interval_time'));
        preparedLabel +=  '\n Sound: ' +  hlp.soundName(meditModel.get('interval_sound'));


        $.meditInterval.applyProperties({
            text: preparedLabel
        });
    }
    //Meditation Sounds names
    var soundLabel = 'Start sound: ' + hlp.soundName(meditModel.get('start_sound'));
    soundLabel  += '\n End sound: ' + hlp.soundName(meditModel.get('end_sound'));
    $.meditSounds.text = soundLabel;

    //Headstart time converted to seconds
    if (meditModel.get('headstart') === -1) {
        $.meditHeadstart.text = 'Off';
    } else {
        $.meditHeadstart.text = hlp.msToTime(meditModel.get('headstart'));
    }
}


/*************************
**Controllers interaction**
**************************/


/**
* Opens sub navigation window in NavigationWindow stack.
* Passes the info about the meditation to the newly opened Window.
*
* @param {string} requestedController - Name of the controller to open.
* @returns {void} - Opens new navigation window stack
*/
function openNavSubView (requestedController, elementID) {
    elementID = elementID || '';
    requestedController = requestedController || '';
    var nav = Alloy.Globals.navMenu;
    var payload = {
            parent: $,
            model: meditModel,
            targetID: elementID
        };

    if (requestedController !== '') {
        //Opening new window in Navigation Windows stack
        if(OS_IOS){
            nav.openWindow(Alloy.createController(requestedController, payload).getView());
        }else{
            //on all others just open a new window
            Alloy.createController(requestedController).show();
        }
    }
}

/*************************
**  On Open Controller  **
**************************/

//Create model
//TODO: check for valid meditationID

    meditModel.fetch({
        id: args.meditationID,
        success: function() {
            updateInterface(meditModel);
        },
        error: function() {
            Ti.API.error('collection failed');
        }
    });

/*************************
**        Exports       **
**************************/
exports.updateInterfaceFromChild = updateInterfaceFromChild;

if (ENV_DEV) {
    //For Specs only
    exports.msToTime = hlp.msToTime;
}