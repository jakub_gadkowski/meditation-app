/*************************
**         Init         **
**************************/
var filesInfo = require('filesInfo');
var hlp = require('helpers');

var sounds = filesInfo.sounds;
var self = $.soundList;
var args = arguments[0] || {};

var data = [];


/*************************
**    Event Listeners   **
**************************/
var closeHandler = function() {
    args.model.save();
    args.parent.updateInterfaceFromChild(args.model);
    hlp.resetSound();
    $.listingTable.removeEventListener('click', handleSoundClick);
	$.soundOptions.removeEventListener('click', clickHandler);
	self.removeEventListener('open', openHandler);
	self.removeEventListener('close', closeHandler);
    //destroy model to prevent memory leaks
    $.destroy();
};

var openHandler = function() {
	listSounds();
    preselectSoundType();
};

var clickHandler = function(e) {
	changeSoundSelection(e.index);
};

self.addEventListener('close', closeHandler);
self.addEventListener('open', openHandler);

//Add event listener to tableRowView
$.listingTable.addEventListener('click', handleSoundClick);
$.soundOptions.addEventListener('click', clickHandler);


/*************************
**    Interface build   **
**************************/

/**
 * Go through the list of available sounds from /lib/filesInfo and create a TableViewRow for each
 * @return {void}
 */
function listSounds () {
	sounds.forEach(function(el) {
		//create payload for controller
        var payload = el;
        //create table row controller and send the payload to it
        var row = Alloy.createController('soundRow', payload).getView();

        //add created table row to the array
        data.push(row);
	});
	$.listingTable.setData(data);
}

/**
 * Go through all TableViewRows on screen and 'deselect' them,
 * then mark selected row and update model to reflect new selection.
 * @param {Object} selectedRow - TableViewRow object
 */
function setSelectedSound (selectedRow) {
	//use already populated `data` array to speed up the process
	data.forEach(function(el){
		var tickMark = el.children[2].children[0];
		if (tickMark.visible === true) {
			tickMark.animate({
			        opacity: 0.0,
			        duration: 250
			    },
		    	function() {
		    		tickMark.setVisible(false);

		    		selectedRow.children[2].children[0].setVisible(true);
					selectedRow.children[2].children[0].animate({
				        opacity: 1.0,
				        duration: 500
				    });
				}
		    );
		}
	});

	updateModelSounds(selectedRow.soundID);
}

/**
 * Preselect sound on interface creation
 * @param  {integer} soundID
 * @return {void}
 */
function preselectSoundRow (soundID) {
	data.forEach(function(el) {
		if (el.soundID === soundID) {
			el.children[2].children[0].setVisible(true);
		}else{
			el.children[2].children[0].setVisible(false);
		}
	});
}

/**
 * Intermediate function that translates TabbedBar label ID into
 * attribute name needed to retrieve parameter from the model
 * @param  {Object} tabSelected - TabbedBar object
 * @return {void}
 */
function changeSoundSelection (tabSelected) {
	var attrName = getSoundType(tabSelected);
	preselectSoundRow(args.model.get(attrName));
}

/**
 * Set default selected option in TabbedBar
 * @return {void}
 */
function preselectSoundType () {
	//Set Interval Sound
	if (args.targetID === 'selectIntervalSound') {
		$.soundOptions.index = 2;
	//Set Start Sound
	} else {
		$.soundOptions.index = 0;
	}
	//Apply change
	changeSoundSelection($.soundOptions.index);
}

/**
 * Process clicked TableViewRow and either select sound or play it.
 * @param  {Object} event - TbaleViewRow Object
 * @return {void}
 */
function handleSoundClick (event) {
    var clickedObj = event.source;
    var soundID = event.row.soundID;
    //Click registered on 'more info' icon
    if (clickedObj.id === 'lblOptions') {
	    hlp.animateButton(clickedObj, {delay: 1000});
		hlp.playSound(soundID);
	// Click registered anywhere else
    } else {
		setSelectedSound(event.row);
    }
}

/*************************
**        Models        **
**************************/

/**
 * Update model parameter for selected sound type with new value
 * @param  {integer} soundID - sound id
 * @return {void}
 */
function updateModelSounds (soundID) {
	soundID = soundID || 0;
	var soundType = $.soundOptions.index;
	var attrName = getSoundType(soundType);

	args.model.set(attrName, soundID);
}

/**
 * Translate the TabbedBar ID into name of the attribute
 * @param  {integer} soundType - range 0-2
 * @return {string} Name of the attribute.
 */
function getSoundType (soundType) {
	var modelAttrName;

	switch (soundType) {
		case 0:
			modelAttrName = 'start_sound';
			break;
		case 1:
			modelAttrName = 'end_sound';
			break;
		case 2:
			modelAttrName = 'interval_sound';
			break;
		default:
			modelAttrName = 'start_sound';
			break;
	}
	return modelAttrName;
}