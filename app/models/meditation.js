exports.definition = {
    config : {
        columns : {
            "meditation_id": "INTEGER PRIMARY KEY AUTOINCREMENT",
            "name" : "TEXT",
            "animation" : "INTEGER", //id
            "time" : "INTEGER", //miliseconds
            "interval_type" : "INTEGER", //Repeated Every/From Start/Before End,
            "interval_time" : "INTEGER", //miliseconds,
            "interval_sound" : "INTEGER", //sound id,
            "start_sound" : "INTEGER", //sound id,
            "end_sound" : "INTEGER", //sound id,
            "headstart" : "INTEGER", //miliseconds
            "type": "INTEGER" //default/builtin/custom/new custom
        },
        adapter : {
            "type" : "sql",
            "collection_name" : "meditations",
            "idAttribute": "meditation_id",
            "db_name" : "gomeditate"
        }
    },

    extendModel: function(Model) {
        _.extend(Model.prototype, {
            // Extend, override or implement Backbone.Model
        });

        return Model;
    },

    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {
            // Extend, override or implement Backbone.Collection
        });

        return Collection;
    }

};