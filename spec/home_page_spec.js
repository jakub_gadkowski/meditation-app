describe("Home Page tests", function() {
  // we need to require alloy so we can access the controllers.
    var Alloy = require("alloy");
	var $;

	beforeEach(function() {
		$ = Alloy.createController("index");
	});

	describe("Test of tests", function(){
		it("tests if testing works",function() {
			expect($.test()).toBe(1);
		});
	});

	describe("Testing getController", function() {
			var clicked;
	 	var emptyController;
		var realObject,realObjectWithParent;

	    it("prepared/fake 'childController' provided ", function() {
	    	clicked = {
			    "childController": "meditationList"
			};
	    	expect($.getController(clicked)).toBe("meditationList");
	  	});

		it("real object provided ", function() {
	    	realObject = $.__views.clickable;
	    	expect($.getController(realObject)).toBe("meditationList");
	  	});

	    it("real object with parent provided ", function() {
	    	realObjectWithParent = $.__views.medLabel;
	    	expect($.getController(realObjectWithParent)).toBe("meditationList");
	  	});

	});

});