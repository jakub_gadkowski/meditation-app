describe("Edit Meditation tests", function() {
  // we need to require alloy so we can access the controllers.
	var Alloy = require("alloy");
	Alloy.Globals.filesInfo = require('filesInfo');

	var $;

	beforeEach(function() {
		$ = Alloy.createController("meditationEdit",{meditationID:1});
	});

	describe("Test of ms conversion into time", function(){
		it("tests if conversion works",function() {
			expect($.msToTime(10000)).not.toBe(null);
		});
	});

	describe("Second test of ms conversion into time", function(){
		it("tests if conversion converts miliseconds into seconds, minutes, hours",function() {
			expect($.msToTime(5000)).toBe('05s');
			expect($.msToTime(10000)).toBe('10s');
			expect($.msToTime(300000)).toBe('05min ');
			expect($.msToTime(600000)).toBe('10min ');
			expect($.msToTime(3600000)).toBe('01h ');
			expect($.msToTime(36000000)).toBe('10h ');
			expect($.msToTime(36000000)).toBe('10h ');
			expect($.msToTime(40271000)).toBe('11h 11min 11s');
		});
	});
});