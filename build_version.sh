#!/usr/bin/env node
var fs = require('fs');

fs.readFile('./tiapp.xml', 'utf8', function (err, data) {
  if (err) throw err;
  incrementBuild(data);
});

function incrementBuild (data) {
	var lastBuild = data.match(/(?!name="build">)[0-9]+(?=<\/property>)/);
	var newBuild = Number(lastBuild) + 1;
	
	var output = data.replace(/(?!>)[0-9]+(?=<\/property>)/, newBuild);
	// // console.log('\033[96m[INFO]\033[0m Last build: '+lastBuild);
	// // console.log('\033[96m[INFO]\033[0m New build: '+newBuild);	
	// console.log('[INFO] Last build: '+lastBuild);
	console.log('[INFO] Build: '+newBuild);	
	
	fs.writeFile('./tiapp.xml', output, function (err) {
  		if (err) throw err;
  		console.log('tiapp.xml saved!');
	});	
}